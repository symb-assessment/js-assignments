# Assignment: Tip Calculator

Create a tip calculator using HTML, CSS, and JavaScript. The application should allow users to enter the bill amount and select a tip percentage, and display the calculated tip amount and total bill including the tip.

## Requirements:

1. The HTML should include input fields for entering the bill amount and selecting the tip percentage, a button to calculate the tip, and a container to display the results.
2. The CSS should style the calculator with a clean and user-friendly design. You can use CSS frameworks like Bootstrap if desired.
3. The JavaScript should handle the logic of calculating the tip amount and total bill based on the input values.
4. When the user enters the bill amount and selects a tip percentage, clicking the "Calculate Tip" button should display the calculated tip amount and the total bill including the tip in the results container.


###### Here's an example of the functionality:

```html
Bill Amount: <input type="number" id="billAmount"><br>
Tip Percentage: 
<select id="tipPercentage">
  <option value="10">10%</option>
  <option value="15">15%</option>
  <option value="20">20%</option>
</select><br>
<button onclick="calculateTip()">Calculate Tip</button>

<div id="results"></div>
```

```javascript
function calculateTip() {
    // Write the logic to run the code
}
```

```css
/* Add your preferred styling for the calculator */
```
