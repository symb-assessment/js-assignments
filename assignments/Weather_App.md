# Assignment: Weather App

Create a weather application using HTML, CSS, and JavaScript. The application should fetch weather data from an API and display it to the user.

## Requirements:

1. The HTML should include an input field for entering a city name, a button to fetch the weather data, and a container to display the weather information.
2. The CSS should style the application with an attractive design. You can use CSS frameworks like Bootstrap if desired.
3. The JavaScript should handle the logic of fetching weather data from an API and updating the UI with the retrieved information.
4. When the user enters a city name and clicks the "Fetch Weather" button, the application should make a request to a weather API (such as OpenWeatherMap or WeatherAPI) and retrieve the current weather data for that city.
5. The retrieved weather information should be displayed on the page, including details like temperature, humidity, wind speed, and weather conditions (e.g., cloudy, sunny, rainy).
6. Handle cases where the API request fails or the entered city name is not found. Display an appropriate error message to the user.

#### Bonus Challenge:

Enhance the application by adding additional features such as displaying a weather icon corresponding to the weather conditions, including a forecast for the upcoming days, or providing an autocomplete feature for the city input field.

**Remember to structure your code neatly and follow best practices for HTML, CSS, and JavaScript. Good luck with your assignment!**

## Weather APIs

There are several popular weather APIs that you can use to fetch weather data for your application. Here are a few options:

1. OpenWeatherMap: OpenWeatherMap provides a wide range of weather data, including current weather conditions, forecasts, and historical data. You can sign up for a free API key on their website: https://openweathermap.org/

2. WeatherAPI: WeatherAPI offers a simple and straightforward API for retrieving weather data. You can sign up for a free API key on their website: https://www.weatherapi.com/

3. AccuWeather: AccuWeather is a well-known weather service that provides accurate and detailed weather information. You can apply for an API key on their developer portal: https://developer.accuweather.com/

4. Weatherstack: Weatherstack is another popular weather API that offers current weather data, forecasts, and historical weather information. You can sign up for a free API key on their website: https://weatherstack.com/

Make sure to review the terms and conditions, usage limits, and pricing (if applicable) for each API provider to determine which one best suits your needs.

Once you have chosen an API, you will typically need to sign up for an account, obtain an API key, and follow the API documentation to learn how to make requests and retrieve the weather data you need for your application.

Please note that the availability and features of these APIs may change over time, so it's always a good idea to visit their respective websites for the most up-to-date information and documentation.
