# Todo Assignment

Task Involved:

- Copy and create this assignment https://www.w3schools.com/howto/howto_js_todolist.asp to your local machine
- Add a dropdown with the todo list, which will be having categories of task. For example If i am adding a personal task, i can select category ‘Personal’ in the category list. 
- You can add any categories in the categories dropdown, but to start with add following list into the category dropdown:
  - Personal
  - Books to read
  - To Buy
- For every task task text and category is required
- In the todo list show the category and the task
- Any type of UI enhancement and improvisation is appreciated
- Upload the code on any Git platform and share the URL 
