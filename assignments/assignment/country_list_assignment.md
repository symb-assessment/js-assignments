# Counties list Assignment


1. Create UI as per this design: https://xd.adobe.com/view/0366b032-b6f8-423a-738e-09d445f7279d-7557/
2. There will be 2 web screens:
    1. Country list screen (index.html)
    2. Country detail screen (detail.html?country=IND)
3. Get the country list using country list API: https://restcountries.com/v3.1/all
4. On country list screen, add search bar as per the UI, which will search the country by name only.
5. On click of "Show Map" open the google map for the country, on google maps. The google map should open in new tab window. Use the maps field in the json (https://goo.gl/maps/WSk3fLwG4vtPQetp7)
6. On click of "Detail" navigate the user to 'Country Detail' (detail.html?country=IND) screen and show the country detail as per UI. Pass the cca3 code in the URL. On the detail page call a new API to get the country detail (https://restcountries.com/v3.1/alpha/ind where ind it the cca3 code of India)
7. Launch the developed appication on [Heroku](https://www.heroku.com/) or [Netlify](https://www.netlify.com/)


Technology: HTML, JS, Ajax, CSS

API Enpoints: 

- To get all the country list: https://restcountries.com/#api-endpoints-v3-all
- To get the detail of a country using cca3 code: https://restcountries.com/#api-endpoints-v3-code
